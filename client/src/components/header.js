import PropTypes from 'prop-types'
import React, {Fragment} from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar'
import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline';

import Logo from './Logo';
import NavMenu from './NavMenu';

const Header = ({ siteTitle }) => (
  <Fragment>
    <CssBaseline />
    <AppBar color="default">
      <Toolbar>
        <Grid container justify="space-around" alignItems="center">
          <Grid item>
            <Logo title={siteTitle} to="/" />  
          </Grid>
          <Grid item>
            <NavMenu />  
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  </Fragment>
);

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: '',
}

export default Header
