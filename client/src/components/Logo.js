import React from 'react';
import {Link} from 'gatsby';
import Grid from '@material-ui/core/Grid';
import logo from '../images/gatsby-icon.png';

export default function Logo({title, to}) {
  return (
    <Link to={to}>
        <Grid container alignItems="center">
          <Grid item>
            <img src={logo} alt="logo" style={{height:'50px'}} />
          </Grid>
          <Grid item style={{marginLeft:'10px'}}>
            {title}
          </Grid>
        </Grid>
      </Link>
  );
};
