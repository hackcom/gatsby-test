import React from 'react';
import {Link} from 'gatsby';
import Grid from '@material-ui/core/Grid';

export default function NavMenu(){
    const menus = [
        {text:"Service", to:"/Service"},
        {text:"Pricing", to:"/Pricing"},
        {text:"Contct Us", to:"/ContactUs"}
    ];
    return (
        <Grid container spacing={24} justify="flex-end">
            {menus.map(m => <Grid key={m.text} item><Link to={m.to}>{m.text}</Link></Grid>)}
        </Grid>
    );
} 


